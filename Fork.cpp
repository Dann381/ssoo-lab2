#include <unistd.h>
#include <sys/wait.h>
#include <iostream>
using namespace std;
#include "Fork.h"

Fork::Fork(){
}

void Fork::Proceso(){
    pid = fork();
}

//Funcion para descargar, extraer y reproducir el audio del video
void Fork::Descarga(char **link){
    //if para validar la creacion del proceso. En caso de haber un error, finaliza el programa
    if (pid < 0) {
        cout << "No se pudo crear el proceso ...";
        cout << endl;
        exit(-1);
    
    //Al crearse el proceso, inicia la descarga del video y extracción del audio
    } else if (pid == 0) { //Inicio del Proceso Hijo
        cout << "Descargando video y extrayendo audio..." << endl;
        //Con el siguiente comando en la terminal descargamos el video, extraemos el audio en formato mp3
        //y cambiamos el nombre del archivo a audio.mp4, que luego quedará como audio.mp3
        execlp("youtube-dl", "youtube-dl", "-x", "--audio-format", "mp3", link[1], "-o", "audio.mp4", NULL);
    
    } else { //Continua con el Proceso Padre
        //El proceso Padre espera por el término del proceso hijo.
        wait (NULL);
        cout << endl;
        cout << "Reproduciendo audio..." << endl;
        //Se reproduce el audio sin la ventana del reproductor. Cambiar "cvlc" por "vlc" para reproducir con la ventana
        execlp("cvlc", "cvlc", "audio.mp3", NULL);
    }
}