#include <unistd.h>
#include <sys/wait.h>
#include <iostream>
using namespace std;
#include "Fork.h"

//Main del programa
int main(int argc, char **argv){
    //En caso de no recibir parametros, finaliza el programa explicando su modo de uso
    if(argc < 2){
        cout << "No se ha especificado dirección del video" << endl;
        cout << "Modo de uso: ./fork1 (link del video a descargar)" << endl;
        return -1;
    }
    //Se instancia la clase en donde estan las funciones con el proceso padre y el proceso hijo
    Fork fork = Fork();
    fork.Proceso();
    //Se guarda el link otorgado por el usuario
    char **link = argv;
    //Se pasa el link a la funcion para descargar, extraer y reproducir el audio
    fork.Descarga(link);

    return 0;
}