# Lab 2 - Sistema Operativo y Redes

Los archivos contenidos en este repositorio consisten en la creación de un programa cuyo propósito es descargar un video de la plataforma Youtube, extraer su audio y reproducirlo en formato mp3.

### Requisitos

Para poder utilizar el programa, es necesario tener previamente instalado VLC media player, y youtube-dl.
Instalación de VLC media player: sudo apt install vlc
Instalación de youtube-dl: sudo apt install youtube-dl

### Iniciar el programa y uso

Para poder iniciar el programa, basta solamente con dirigirse a la ruta en la cual se encuentra mediante la terminal usando el comando cd. Luego, se debe escribir el comando "make" en la terminal para compilar los archivos, y el programa ya estará listo para utilizarse. Para poder usarlo, simplemente se debe usar el comando "./programa (link del video a descargar)". Ejemplo: ./programa https://www.youtube.com/watch?v=dQw4w9WgXcQ

### Construido con

El presente proyecto de programación, se construyó en base al lenguaje de programación C++, en conjunto con el IDE "Visual Studio Code".

* [C++] - Lenguaje de programación utilizado.
* [Visual Studio Code](https://code.visualstudio.com/) - IDE utilizado.

### Autores

* **Daniel Tobar** - Estudiante de Ingeniería Civil en Bioinformática.
