#ifndef FORK_H
#define FORK_H

class Fork{
    private:
        pid_t pid;
    public:
        Fork();
        void Proceso();
        void Descarga(char **link);
};
#endif